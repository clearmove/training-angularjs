var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var request = require('request');
//var jade = require('jade');
app.engine('jade', require('jade').__express);

app.use(express.static(__dirname + '/'));
//app.use('/test123',express.static(__dirname + '/test.html'));
//app.use('/news', express.static(__dirname + '/news.html'));
//app.use('/login', express.static(__dirname + '/login.html'));


app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
    'extended': 'true'
}));

app.use(bodyParser.json());
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));

app.use(methodOverride());

app.listen(3000);
console.log("App listening on port 3000");
