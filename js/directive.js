app.directive('mainMenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'template/main_menu.html'
    };
});

app.directive('err', function() {
    return {
        restrict: "EA",
        templateUrl: 'template/err.html',
        scope: {
            'msg': '='
        }
    };
});
