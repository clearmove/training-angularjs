app.controller("newsController", function($scope, Restangular) {

    $scope.currentPage = 1;

    Restangular.all('category').customGET().then(function(res) {
        $scope.categories = res.results[0];
    }, function(err) {
        console.error('err', err);
    });

});
