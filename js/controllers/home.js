app.controller("homeController", function($scope, $sce, $filter, user, $rootScope) {
    $rootScope.test = true;
    $rootScope.msg = "home error";
    $scope.users = [{
        "id": 1,
        "name": "test1",
        "status": 0,
        "location": "",
        "cars": [{
            "id": "c1",
            "name": "car1",
            //  "img": ""
        }, {
            "id": "c11",
            "name": "car11",
            //"img": ""
        }]
    }, {
        "id": 2,
        "name": "test2",
        "status": 1,
        "location": "amman",
        "cars": [{
            "id": "c2",
            "name": "car2",
            "img": "imgs/car1.png"
        }, {
            "id": "c22",
            "name": "car22",
            "img": "imgs/car2.jpg"
        }]


    }, {
        "id": 3,
        "name": "test3",
        "status": 2,
        "location": "amman2",
        "cars": [{
            "id": "c3",
            "name": "car3",
            //  "img": ""
        }, {
            "id": "c33",
            "name": "car33",
            //  "img": ""
        }]
    }];

    $scope.getLocation = function(loc) {

        $scope.fil1 = ($filter("locationFilter")(loc));

        return $scope.fil1;
    };

});
