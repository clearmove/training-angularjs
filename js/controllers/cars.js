app.controller("carsController", function($scope, Cart) {

    $scope.cars = [{
        "id": 1,
        "name": "car1",
        "price": 11,
        "img": "imgs/car1.png"

    }, {
        "id": 2,
        "name": "car2",
        "price": 22,
        "img": "imgs/car2.jpg"


    }, {
        "id": 3,
        "name": "car3",
        "price": 33,
        "img": "imgs/car3.jpg"
    }];

    $scope.cart = Cart.get();


    $scope.add = function(car) {

        Cart.add($scope.cart.totalAmount, car.price).then(function(res) {
            $scope.cart.cars.push(car);
            $scope.cart.totalAmount = res.totalAmount;
            Cart.update($scope.cart);
        }, function(err) {
            console.log('err', err);
        });

    };

    $scope.remove = function(index, price) {
        Cart.remove($scope.cart.totalAmount, price).then(function(res) {
            $scope.cart.totalAmount = res.totalAmount;
            $scope.cart.cars.splice(index, 1);
            Cart.update($scope.cart);
        }, function(err) {

        });
    };

    $scope.removeAll = function() {
        $scope.cart = Cart.removeAll();
    };

});
