app.controller("signinController", function($scope, Restangular) {

    $scope.signin = function(form) {
        console.log('form', form);
        if (form.$valid) {

            Restangular.all('signin').post({
                'email': form.email.$viewValue,
                'password': form.password.$viewValue
            }).then(function(res) {
                $scope.message = res.message;
                $scope.class = "success";
                console.log('res', res);
            }, function(err) {
                $scope.message = err.data.message;
                $scope.class = "danger";
                console.error('err', err);
            });

        }
    };
});
