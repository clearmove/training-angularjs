app.controller("chatController", function($scope, $rootScope, Chat) {
    $rootScope.test = false;
    $rootScope.msg = "chat error";
    $scope.chats = Chat.get();

    $scope.send = function(form) {
        $scope.chats.push({
            "date": new Date(),
            "text": $scope.chat,
            "id": (form.Id) ? form.Id.$viewValue : 0
        });
        $scope.chat = "";
        Chat.update($scope.chats);
    };
});
