app.filter('statusFilter', function($sce) {
    return function(statusFilter) {
        switch (statusFilter) {
            case 0:
                return $sce.trustAsHtml('<i class="glyphicon glyphicon-remove"></i>');
                break;
            case 1:
                return $sce.trustAsHtml('<i class="glyphicon glyphicon-check"></i>');
                break;
            case 2:
                return $sce.trustAsHtml('<i class="glyphicon glyphicon-info-sign"></i>');
                break;
            default:
        }
    }
}).filter('locationFilter', function() {
    return function(location) {

        if (location)
            return location;
        else
            return 'No location';
    }

});
