app.service("Cart", function($cookies, $q) {

    var get = function() {
        return ($cookies.getObject('cart') && $cookies.getObject('cart') !== undefined) ? JSON.parse($cookies.getObject('cart')) : {
            "totalAmount": 0,
            "cars": []
        };
    };

    var update = function(cart) {
        return ($cookies.putObject('cart', JSON.stringify(cart)));
    };

    var add = function(totalAmount, price) {
        return $q(function(resolve, reject) {
            totalAmount = totalAmount + price;

            resolve({
                "totalAmount": totalAmount
            });
        });
    };

    var remove = function(totalAmount, price) {
        return $q(function(resolve, reject) {
            totalAmount = totalAmount - price;
            resolve({
                "totalAmount": totalAmount
            });
        });

    };

    var removeAll = function() {
        $cookies.remove('cart');
        return {
            "totalAmount": 0,
            "cars": []
        };

    };

    return {
        "add": add,
        "remove": remove,
        "get": get,
        "update": update,
        "removeAll": removeAll
    }
});
