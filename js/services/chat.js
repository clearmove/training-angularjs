app.service("Chat", function($cookies) {

    var get = function() {
        return ($cookies.getObject('chats') && $cookies.getObject('chats') !== undefined) ? JSON.parse($cookies.getObject('chats')) : [];
    };

    var update = function(chats) {
        return ($cookies.putObject('chats', JSON.stringify(chats)));
    };

    return {
        "get": get,
        "update": update
    }
});
