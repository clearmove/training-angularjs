app.config(['$stateProvider', '$urlRouterProvider', 'RestangularProvider', function($stateProvider, $urlRouterProvider, RestangularProvider) {


    RestangularProvider.setBaseUrl('http://192.168.1.143:4000');

    RestangularProvider.setDefaultHttpFields({
        withCredentials: true
    });

    RestangularProvider.setResponseExtractor(function(response, operation) {
        if (operation === 'getList') {
            var newResponse = response;
            if (response.results) {
                newResponse = response.results[0].data;
                if ('meta' in response.results[0])
                    newResponse.meta = response.results[0].meta;
            } else {
                newResponse = response.data;
            }
            return newResponse;
        }
        return response;
    });

    // RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
    //     if (operation === 'put') {
    //         elem._id = undefined;
    //         return elem;
    //     }
    //     return elem;
    // })

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('home', {
            url: '/',
            controller: "homeController",
            templateUrl: 'view/home.html',
        })
        .state('signin', {
            url: '/signin',
            controller: "signinController",
            templateUrl: 'view/signin.html',
        })
        .state('cars', {
            url: '/cars',
            controller: "carsController",
            templateUrl: 'view/cars.html',
        })
        .state('news', {
            url: '/news/:id',
            controller: "newsController",
            templateUrl: 'view/news.html',
        })
        .state('chat', {
            url: '/chat',
            controller: "chatController",
            templateUrl: 'view/chat.html',
        })
        .state('contactUs', {
            url: '/contact-us',
            controller: "contactUsController",
            templateUrl: 'view/contact_us.html',
        });


}]);
